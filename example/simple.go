package main

import (
	cx "context"
	fl "flag"
	"fmt"
	"os"

	"gitlab.com/clbr/subcmd"
)

// wallet subcommand
type wal struct {
	cfgDir string
}

func (*wal) Name() string     { return "wallet" }
func (*wal) Synopsis() string { return "Runs Calibrae wallet" }
func (*wal) Usage() string {
	return `Calibrae wallet usage:
`
}
func (parent *wal) SetFlags(flags *fl.FlagSet) {
	flags.StringVar(&parent.cfgDir, "d", "~/calibrae", "data directory")
}
func (parent *wal) Execute(
	_ cx.Context, flags *fl.FlagSet, _ ...interface{}) subcmd.ExitStatus {

	// here we spawn the entry point of our subcommand
	fmt.Printf("Starting Wallet with cfgDir %s\n", parent.cfgDir)
	// Note that we can in fact place the entire
	// subcommand executable block here
	// How to format the parameters of the subcommand:
	// wallet.Init(&parent.cfgDir)
	return subcmd.ExitSuccess
}

func main() {
	// the actual subcommand
	subcmd.Register(&wal{}, "interface")
	subcmd.Register(subcmd.Help(), "subcommands")
	subcmd.Register(subcmd.Flags(), "subcommands")
	subcmd.Register(subcmd.Commands(), "subcommands")

	fl.Parse()
	ctx := cx.Background()
	os.Exit(int(subcmd.Execute(ctx)))
}
