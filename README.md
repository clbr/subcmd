# subcmd

A fork of https://github.com/google/subcommands with bad names and stutter removed. This is the equivalent of the go standard library 'flag' with the necessary and very useful subcommand functionality added.

This is the first in a set of rewrites of common standard go libraries that adopts a common sense application of many principles of clean coding to enable the code to be in fact the documentation itself, it is so simple and clear.

See [example.go](example/simple.go) to learn how to use this library, as well as reading [main.go](main.go) itself