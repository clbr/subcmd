// Package subcmd (Subcommand) provides commandline args and subcommands.
// taken from https://github.com/google/subcommands and changed to
// remove some really bad stutter and comment-fluff
package subcmd

import (
	"fmt"
	"io"
	"os"
	"path"
	"sort"

	"gitlab.com/clbr/flag"
	"gitlab.com/clbr/proc"
)

type Command interface {
	Name() string
	Synopsis() string
	Usage() string
	WriteFlags(*flag.Set)
	Execute(ctx proc.Context, f *flag.Set, args ...interface{}) ExitStatus
}

type Commander struct {
	commands  []*commandGroup
	topFlags  *flag.Set
	important []string
	name      string
	Output    io.Writer
	Error     io.Writer
}

type commandGroup struct {
	name     string
	commands []Command
}

type ExitStatus int

const (
	ExitSuccess ExitStatus = iota
	ExitFailure
	ExitUsageError
)

func NewCommander(topLevelFlags *flag.Set, name string) *Commander {
	cdr := &Commander{
		topFlags: topLevelFlags,
		name:     name,
		Output:   os.Stdout,
		Error:    os.Stderr,
	}
	topLevelFlags.Usage = func() { cdr.explain(cdr.Error) }
	return cdr
}

func (cdr *Commander) Register(cmd Command, group string) {
	for _, g := range cdr.commands {
		if g.name == group {
			g.commands = append(g.commands, cmd)
			return
		}
	}
	cdr.commands = append(cdr.commands, &commandGroup{
		name:     group,
		commands: []Command{cmd},
	})
}

func (cdr *Commander) ImportantFlag(name string) {
	cdr.important = append(cdr.important, name)
}

func (cdr *Commander) Execute(ctx proc.Context, args ...interface{}) ExitStatus {
	if cdr.topFlags.NArg() < 1 {
		cdr.topFlags.Usage()
		return ExitUsageError
	}

	name := cdr.topFlags.Arg(0)

	for _, group := range cdr.commands {
		for _, cmd := range group.commands {
			if name != cmd.Name() {
				continue
			}
			f := flag.NewFlagSet(name, flag.ContinueOnError)
			f.Usage = func() { explain(cdr.Error, cmd) }
			cmd.WriteFlags(f)
			if f.Parse(cdr.topFlags.Args()[1:]) != nil {
				return ExitUsageError
			}
			return cmd.Execute(ctx, f, args...)
		}
	}

	cdr.topFlags.Usage()
	return ExitUsageError
}

type byGroupName []*commandGroup

func (p byGroupName) Len() int           { return len(p) }
func (p byGroupName) Less(i, j int) bool { return p[i].name < p[j].name }
func (p byGroupName) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

func (cdr *Commander) explain(w io.Writer) {
	fmt.Fprintf(w, "Usage: %s <flags> <subcommand> <subcommand args>\n\n", cdr.name)
	sort.Sort(byGroupName(cdr.commands))
	for _, group := range cdr.commands {
		explainGroup(w, group)
	}
	if cdr.topFlags == nil {
		fmt.Fprintln(w, "\nNo top level flags.")
		return
	}
	if len(cdr.important) == 0 {
		fmt.Fprintf(w, "\nUse \"%s flags\" for a list of top-level flags\n", cdr.name)
		return
	}

	fmt.Fprintf(w, "\nTop-level flags (use \"%s flags\" for a full list):\n", cdr.name)
	for _, name := range cdr.important {
		f := cdr.topFlags.Lookup(name)
		if f == nil {
			panic("Important flag is not defined")
		}
		fmt.Fprintf(w, "  -%s=%s: %s\n", f.Name, f.DefValue, f.Usage)
	}
}

func (g commandGroup) Len() int           { return len(g.commands) }
func (g commandGroup) Less(i, j int) bool { return g.commands[i].Name() < g.commands[j].Name() }
func (g commandGroup) Swap(i, j int)      { g.commands[i], g.commands[j] = g.commands[j], g.commands[i] }

func explainGroup(w io.Writer, group *commandGroup) {
	if len(group.commands) == 0 {
		return
	}
	if group.name == "" {
		fmt.Fprintf(w, "Subcommands:\n")
	} else {
		fmt.Fprintf(w, "Subcommands for %s:\n", group.name)
	}
	// sort.Sort(group)
	for _, cmd := range group.commands {
		fmt.Fprintf(w, "\t%-15s  %s\n", cmd.Name(), cmd.Synopsis())
	}
	fmt.Fprintln(w)
}

func explain(w io.Writer, cmd Command) {
	fmt.Fprintf(w, "%s", cmd.Usage())
	subflags := flag.NewFlagSet(cmd.Name(), flag.PanicOnError)
	subflags.SetOutput(w)
	cmd.WriteFlags(subflags)
	subflags.PrintDefaults()
}

type helper Commander

func (h *helper) Name() string         { return "help" }
func (h *helper) Synopsis() string     { return "describe subcommands and their syntax" }
func (h *helper) WriteFlags(*flag.Set) {}
func (h *helper) Usage() string {
	return `help [<subcommand>]:
	With an argument, prints detailed information on the use of	the specified subcommand. 
	With no argument, print a list of all commands and a brief description of each.
`
}
func (h *helper) Execute(_ proc.Context, f *flag.Set, args ...interface{}) ExitStatus {
	switch f.NArg() {
	case 0:
		(*Commander)(h).explain(h.Output)
		return ExitSuccess

	case 1:
		for _, group := range h.commands {
			for _, cmd := range group.commands {
				if f.Arg(0) != cmd.Name() {
					continue
				}
				explain(h.Output, cmd)
				return ExitSuccess
			}
		}
		fmt.Fprintf(h.Error, "Subcommand %s not understood\n", f.Arg(0))
	}

	f.Usage()
	return ExitUsageError
}

func (cdr *Commander) Help() Command {
	return (*helper)(cdr)
}

type flagger Commander

func (flg *flagger) Name() string         { return "flags" }
func (flg *flagger) Synopsis() string     { return "describe all known top-level flags" }
func (flg *flagger) WriteFlags(*flag.Set) {}
func (flg *flagger) Usage() string {
	return `flags [<subcommand>]:
	With an argument, print all flags of <subcommand>. Else,
	print a description of all known top-level flags.  (The basic
	help information only discusses the most generally important
	top-level flags.)
`
}
func (flg *flagger) Execute(_ proc.Context, f *flag.Set, _ ...interface{}) ExitStatus {
	if f.NArg() > 1 {
		f.Usage()
		return ExitUsageError
	}

	if f.NArg() == 0 {
		if flg.topFlags == nil {
			fmt.Fprintln(flg.Output, "No top-level flags are defined.")
		} else {
			flg.topFlags.PrintDefaults()
		}
		return ExitSuccess
	}

	for _, group := range flg.commands {
		for _, cmd := range group.commands {
			if f.Arg(0) != cmd.Name() {
				continue
			}
			subflags := flag.NewFlagSet(cmd.Name(), flag.PanicOnError)
			subflags.SetOutput(flg.Output)
			cmd.WriteFlags(subflags)
			subflags.PrintDefaults()
			return ExitSuccess
		}
	}
	fmt.Fprintf(flg.Error, "Subcommand %s not understood\n", f.Arg(0))
	return ExitFailure
}

func (cdr *Commander) Flags() Command {
	return (*flagger)(cdr)
}

type lister Commander

func (l *lister) Name() string         { return "commands" }
func (l *lister) Synopsis() string     { return "list all command names" }
func (l *lister) WriteFlags(*flag.Set) {}
func (l *lister) Usage() string {
	return `commands:
	Print a list of all commands.
`
}
func (l *lister) Execute(_ proc.Context, f *flag.Set, _ ...interface{}) ExitStatus {
	if f.NArg() != 0 {
		f.Usage()
		return ExitUsageError
	}

	for _, group := range l.commands {
		for _, cmd := range group.commands {
			fmt.Fprintf(l.Output, "%s\n", cmd.Name())
		}
	}
	return ExitSuccess
}

func (cdr *Commander) Commands() Command {
	return (*lister)(cdr)
}

var DefaultCommander *Commander

func init() {
	DefaultCommander = NewCommander(flag.CommandLine, path.Base(os.Args[0]))
}

func Register(cmd Command, group string) {
	DefaultCommander.Register(cmd, group)
}

func ImportantFlag(name string) {
	DefaultCommander.ImportantFlag(name)
}

func Execute(ctx proc.Context, args ...interface{}) ExitStatus {
	return DefaultCommander.Execute(ctx, args...)
}

func Help() Command {
	return DefaultCommander.Help()
}

func Flags() Command {
	return DefaultCommander.Flags()
}

func Commands() Command {
	return DefaultCommander.Commands()
}
